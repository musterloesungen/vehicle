public class Vehicle {
    String model;
    double speed;
    boolean canTow;

    // Konstruktor zum Initialisieren der Attribute
    public Vehicle(String model, double speed, boolean canTow) {
        this.model = model;
        this.speed = speed;
        this.canTow = canTow;
    }

    // Methode zur Ausgabe der Fahrzeuginformationen
    public void displayInfo() {
        System.out.println("Modell: " + model);
        System.out.println("Geschwindigkeit: " + speed);
        System.out.println("Kann ziehen: " + canTow);
    }

    // Hauptmethode
    public static void main(String[] args) {
        // Array von Vehicle-Objekten erstellen
        Vehicle[] vehicles = new Vehicle[3];

        // Array mit verschiedenen Fahrzeugtypen füllen
        vehicles[0] = new Vehicle("Sedan", 180.0, false);
        vehicles[1] = new Vehicle("Truck", 120.0, true);
        vehicles[2] = new Vehicle("SUV", 150.0, true);

        // Informationen aller Fahrzeuge ausgeben
        for (Vehicle vehicle : vehicles) {
            vehicle.displayInfo();
            System.out.println("---");
        }
    }
}
