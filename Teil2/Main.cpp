// Basisklasse Vehicle
public class Vehicle {
    String model;
    double speed;

    // Konstruktor der Basisklasse
    public Vehicle(String model, double speed) {
        this.model = model;
        this.speed = speed;
    }

    // Methode zur Ausgabe der allgemeinen Fahrzeuginformationen
    public void displayInfo() {
        System.out.println("Modell: " + model);
        System.out.println("Geschwindigkeit: " + speed);
    }
}

// Abgeleitete Klasse TowTruck
public class TowTruck extends Vehicle {
    double towingCapacity;

    // Konstruktor der abgeleiteten Klasse
    public TowTruck(String model, double speed, double towingCapacity) {
        super(model, speed);
        this.towingCapacity = towingCapacity;
    }

    // Überschriebene Methode zur Ausgabe der erweiterten Fahrzeuginformationen
    @Override
    public void displayInfo() {
        super.displayInfo();
        System.out.println("Zugkapazität: " + towingCapacity);
    }
}

// Hauptmethode
public class Main {
    public static void main(String[] args) {
        // Array von Vehicle-Objekten (Polymorphie)
        Vehicle[] vehicles = new Vehicle[4];

        // Array mit verschiedenen Fahrzeugtypen füllen
        vehicles[0] = new Vehicle("Sedan", 180.0);
        vehicles[1] = new TowTruck("Truck", 120.0, 2000.0);
        vehicles[2] = new Vehicle("SUV", 150.0);
        vehicles[3] = new TowTruck("Heavy Truck", 100.0, 5000.0);

        // Informationen aller Fahrzeuge ausgeben
        for (Vehicle vehicle : vehicles) {
            vehicle.displayInfo();
            System.out.println("---");
        }
    }
}
