### Aufgabe: Fahrzeugparkverwaltung

#### Teil 1: Spezialisierung durch Attribute

**Ziel**: Erstelle ein Java-Programm zur Verwaltung eines Fahrzeugparks. Jedes Fahrzeug hat bestimmte allgemeine Eigenschaften (z.B. Modellname, Geschwindigkeit). Einige Fahrzeuge sind jedoch spezialisiert; beispielsweise können manche Fahrzeuge Anhänger ziehen.

1. Erstelle eine Klasse `Vehicle` mit den Attributen:
    - `String model`
    - `double speed`
    - `boolean canTow`

2. Erstelle eine Methode `displayInfo()`, die die Fahrzeuginformationen ausgibt.

3. Die Hauptfunktion (`main`) soll mehrere Objekte der Klasse `Vehicle` erzeugen und in einem Array speichern. Anschließend sollen die Informationen aller Fahrzeuge ausgegeben werden.

```java
public class Vehicle {
    String model;
    double speed;
    boolean canTow;

    public void displayInfo() {
        // Informationen ausgeben
    }
}
```

#### Teil 2: Spezialisierung durch Vererbung

**Ziel**: Erweitere das Programm aus Teil 1, sodass die Spezialisierung der Fahrzeuge durch Vererbung dargestellt wird.

1. Die Klasse `Vehicle` bleibt unverändert und stellt die Basisklasse dar.

2. Erstelle eine neue Klasse `TowTruck`, die von `Vehicle` erbt und das Attribut `double towingCapacity` hinzufügt.

3. Überschreibe die Methode `displayInfo()` in der Klasse `TowTruck`, um auch die zusätzliche Eigenschaft auszugeben.

4. Ändere die Hauptfunktion (`main`), um sowohl `Vehicle`- als auch `TowTruck`-Objekte zu erzeugen und in einem Array vom Typ `Vehicle` zu speichern. Nutze Polymorphismus, um die Informationen aller Fahrzeuge auszugeben.

```java
public class TowTruck extends Vehicle {
    double towingCapacity;

    @Override
    public void displayInfo() {
        // Erweiterte Informationen ausgeben
    }
}
```

#### Hinweis

In Teil 1 wird die Spezialisierung durch ein zusätzliches Attribut in der Basisklasse dargestellt. In Teil 2 wird die Spezialisierung durch Vererbung realisiert, und es werden mehrere Objekte verschiedener, aber verwandter Klassen in einem Array gespeichert und verarbeitet.
